/**
 * @format
 */

import { AppRegistry } from 'react-native';
import React from 'react';
import App from './App';
import { name as appName } from './app.json';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import provinceReducer from './src/reducers/provinceReducer'

import Detail from './src/components/Detail'
import Track from './src/components/Track'

const store = createStore(provinceReducer);

const AppContainer = () =>
    <Provider store={store}>
        <App />
    </Provider>

AppRegistry.registerComponent(appName, () => AppContainer);
