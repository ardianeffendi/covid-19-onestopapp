import React, { useState, useEffect } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Dimensions,
    StatusBar, Image, TouchableOpacity, FlatList
} from 'react-native';
import { BoxShadow } from 'react-native-shadow';
import Axios from 'axios';
import { useNavigation } from '@react-navigation/native';


const width = Math.round(Dimensions.get('window').width);
const height = Math.round(Dimensions.get('window').height);



export default function Track() {
    const navigation = useNavigation();
    const [data, setData] = useState([
        {
            data: {},
            isLoading: true,
            isError: false
        }
    ])

    useEffect(() => {
        getCoronaData()
    }, [])

    let getCoronaData = async () => {
        try {
            const response = await Axios.get(`https://api.kawalcorona.com/indonesia/provinsi`);
            setData({ isError: false, isLoading: true, data: response.data })

        } catch (error) {
            setData({ isLoading: false, isError: true })
        }
    }

    return (
        <View style={styles.container}>
            <StatusBar translucent barStyle="light-content" />

            <View style={styles.headerBar}>
                <Text style={styles.titleCovid}>COVID-19</Text>
                <Text style={styles.titleTrackMe}>Track Me</Text>
                <Image style={styles.searchToogle} source={require('../assets/search.png')} />
            </View>

            <View style={styles.body}>

                <FlatList
                    keyExtractor={({ FID }, index) => index.toString()}
                    data={data.data}
                    renderItem={({ item }) => (
                        <View style={{
                            justifyContent: 'center', alignItems: 'center',
                            marginTop: 20
                        }}>
                            <BoxShadow setting={shadowOpt}>
                                <View style={{ maxWidth: width * 0.6 }}>
                                    <Text numberOfLines={1}
                                        style={{ flex: 1, fontSize: 24, fontWeight: 'bold', marginStart: 18 }}
                                    >{item.attributes.Provinsi}</Text>
                                </View>
                                <Text
                                    style={{
                                        position: "absolute", marginTop: 40,
                                        fontWeight: 'bold', fontSize: 18, marginStart: 18
                                    }}
                                >Total Kasus: {item.attributes.Kasus_Posi}</Text>
                                <TouchableOpacity
                                    onPress={() => { }}
                                    style={styles.arrow}>
                                    <Image source={require('../assets/arrow.png')} />
                                </TouchableOpacity>

                            </BoxShadow>
                        </View>
                    )}
                />

            </View>

        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
        marginTop: StatusBar.currentHeight || 0,
    },
    headerBar: {
        flexDirection: 'row',
        padding: 4,
        backgroundColor: '#FDD',
        maxHeight: height * 0.08
    },
    searchToogle: {
        position: 'absolute',
        width: 28,
        height: 28,
        marginTop: height * 0.02,
        marginStart: width * 0.9
    },
    titleCovid: {
        color: '#E02020',
        marginEnd: 14,
        marginTop: 6,
        marginStart: width * 0.1,
        fontSize: 24,
        fontWeight: 'bold',
    },
    titleTrackMe: {
        marginTop: height * 0.02,
        fontSize: 16
    },
    body: {
        flex: 1,

    },
    arrow: {
        position: 'absolute',
        marginStart: width * 0.65,
        marginTop: 16
    }

})

const shadowOpt = {
    width: 325,
    height: 88,
    color: "#000",
    border: 5,
    radius: 8,
    opacity: 0.2,
    x: 0,
    y: 0,
    style: { marginVertical: 5, flexDirection: 'row' }
};



