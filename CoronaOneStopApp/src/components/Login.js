import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    Dimensions,
    StatusBar, Image, TextInput, Button, TouchableOpacity, KeyboardAvoidingView
} from 'react-native';

import { useNavigation, useRoute } from '@react-navigation/native';

const width = Math.round(Dimensions.get('window').width);
const height = Math.round(Dimensions.get('window').height);


const loginHandler = () => {

}

const Login = () => {
    const navigation = useNavigation();

    return (
        <View style={styles.container}>
            <StatusBar translucent barStyle="light-content" />
            <View style={styles.header}>
                <Image
                    style={{
                        width: 216, height: 56, marginTop: 8,
                        marginStart: 20, resizeMode: 'contain'
                    }}
                    source={require('../assets/TxtHeader.png')} />
                <Image
                    style={{ marginStart: 24, marginTop: 8 }}
                    source={require('../assets/Separator.png')} />
                <Image
                    style={{ position: 'absolute', marginStart: width * 0.7, marginTop: 25 }}
                    source={require('../assets/TxtHeaderSmall.png')} />
            </View>
            <Text
                style={{
                    fontSize: 24, fontWeight: "bold",
                    marginBottom: 32, alignSelf: 'center', marginTop: 40
                }}
            >Sign In</Text>

            <View style={styles.signin}>
                <TextInput placeholder={"Email"}
                    style={{ fontWeight: 'bold', fontSize: 30, borderBottomWidth: 2, marginBottom: 12 }} />
                <TextInput placeholder={"Password"}
                    style={{ fontWeight: 'bold', fontSize: 30, borderBottomWidth: 2, marginBottom: 48 }} />
                <Button
                    color={"black"}
                    title={"LOGIN"} />
                <TouchableOpacity onPress={() => onPress = navigation.navigate('Track')}>
                    <Text style={{ fontSize: 16, alignSelf: 'center', marginTop: 12 }}>Guest</Text>
                </TouchableOpacity>
            </View>

            <View style={{ borderTopWidth: 1, width: width * 0.9, alignSelf: 'center', }}>
                <Text style={{ alignSelf: 'center' }}>About Us</Text>
            </View>



        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#DAD6CB',
        marginTop: StatusBar.currentHeight || 0,
    },
    header: {
        flexDirection: 'row',
        padding: 8,
        marginTop: 42,
        maxHeight: height * 0.16,
        marginStart: 12,
        marginEnd: 12,
        borderBottomWidth: 2
    },
    signin: {
        flex: 1,
        padding: 8,
        maxHeight: height * 0.5,
        paddingStart: 48,
        paddingEnd: 48,
        margin: 12,

    }
});

export default Login;