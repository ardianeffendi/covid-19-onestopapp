import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    Dimensions,
    StatusBar, Image, TextInput, Button, TouchableOpacity
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';

const width = Math.round(Dimensions.get('window').width);
const height = Math.round(Dimensions.get('window').height);


export default Detail = ({ navigation }) => {
    console.log(navigation.getParam('Provinsi'))

    return (
        <View style={styles.container}>
            <StatusBar translucent barStyle="light-content" />

            <View style={styles.headerBar}>
                <TouchableOpacity onPress={() => onPress = navigation.goBack()}>
                    <Icon style={{ marginTop: 8 }} name="arrow-back" size={32} />
                </TouchableOpacity>
                <Text style={styles.titleCovid}>COVID-19</Text>
                <Text style={styles.titleTrackMe}>Detail Page</Text>
            </View>

            <View style={styles.body}>
                <View style={styles.boxBodyTop}>
                    <Text style={styles.textInBox}></Text>
                </View>
                <View style={styles.line} />

                <View style={styles.bodyBox}>

                    <View style={styles.rowData}>
                        <Icon name="insert-chart" size={80} />
                        <View style={{ alignItems: 'center', marginTop: 5 }}>
                            <Text style={styles.fontData}>Total Cases</Text>
                            <Text style={styles.fontData}>0</Text>
                        </View>
                    </View>
                    <View style={styles.rowData}>
                        <Icon name="add-box" size={80} />
                        <View style={{ alignItems: 'center', marginTop: 5 }}>
                            <Text style={styles.fontData}>Positive</Text>
                            <Text style={styles.fontData}>0</Text>
                        </View>
                    </View>
                    <View style={styles.rowData}>
                        <Icon name="assignment-turned-in" size={80} />
                        <View style={{ alignItems: 'center', marginTop: 5 }}>
                            <Text style={styles.fontData}>Negative</Text>
                            <Text style={styles.fontData}>0</Text>
                        </View>
                    </View>

                </View>

                <View style={styles.footer}>
                    <Text style={styles.footerText}>Deaths</Text>
                    <Text style={styles.footerText}>0</Text>
                </View>
            </View>

        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
        marginTop: StatusBar.currentHeight || 0,
    },
    headerBar: {
        flex: 1,
        flexDirection: 'row',
        padding: 4,
        backgroundColor: '#FDD',
        maxHeight: height * 0.08,

    },
    titleCovid: {
        color: '#E02020',
        marginEnd: 14,
        marginTop: 8,
        marginStart: width * 0.02,
        fontSize: 24,
        fontWeight: 'bold',
    },
    titleTrackMe: {
        marginTop: height * 0.02,
        fontSize: 16
    },
    body: {
        flex: 1,
        alignItems: 'center',

    },
    boxBodyTop: {
        backgroundColor: "#F2F2F2",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 6,
        paddingVertical: 30,
        paddingHorizontal: 90,
        borderRadius: 8,
        marginTop: 36,
    },
    textInBox: {
        fontSize: 24,
        fontWeight: 'bold'
    },
    line: {
        backgroundColor: "#F2F2F2",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 6,
        paddingVertical: 0,
        paddingHorizontal: 180,
        borderRadius: 8,
        marginTop: 40,
        borderWidth: 2
    },
    bodyBox: {
        backgroundColor: "#F2F2F2",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 6,
        borderRadius: 8,
        marginTop: 20,
        borderWidth: 2,
        width: width * 0.9,

    },
    rowData: {
        flexDirection: 'row',

    },
    fontData: {
        fontSize: 20, fontWeight: 'bold'
    },
    footer: {
        alignItems: 'center',
        backgroundColor: "#D95757",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 6,
        borderRadius: 8,
        marginTop: 20,
        borderWidth: 2,
        width: width * 0.9,
        height: 90
    },
    footerText: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#FFF',
        borderColor: '#000',

    }
})

