import React from 'react';
import Track from './src/components/Track';
import Detail from './src/components/Detail';
import Login from './src/components/Login';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

const Stack = createStackNavigator();
const BottomTabs = createBottomTabNavigator();

const createBottomTabs = () => {
  return <BottomTabs.Navigator>
    <BottomTabs.Screen name="Track" component={Track} />
    <BottomTabs.Screen name="Detail" component={Detail} />
  </BottomTabs.Navigator>
}

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
        <Stack.Screen name="Track" children={createBottomTabs} options={{ headerShown: false }} />
        <Stack.Screen name="Detail" component={Detail} options={{ headerShown: false }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
